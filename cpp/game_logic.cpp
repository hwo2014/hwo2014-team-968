#include "game_logic.h"
#include "protocol.h"

using namespace hwo_protocol;

game_logic::game_logic()
	: action_map
{
	{ "join", &game_logic::on_join }
	, { "gameStart", &game_logic::on_game_start }
	, { "carPositions", &game_logic::on_car_positions }
	, { "crash", &game_logic::on_crash }
	, { "gameEnd", &game_logic::on_game_end }
	, { "error", &game_logic::on_error }
	, { "yourCar", &game_logic::on_your_car }
	, { "gameInit", &game_logic::on_game_init }
	, { "spawn", &game_logic::on_spawn }
	, { "lapFinished", &game_logic::on_lap_finished }
	, { "finish", &game_logic::on_finish }
	, { "tournamentEnd", &game_logic::on_tournament_end }
}
{
}

game_logic::msg_vector game_logic::react( const jsoncons::json& msg )
{
	const auto& msg_type = msg["msgType"].as<std::string>();
	const auto& data = msg["data"];
	auto action_it = action_map.find( msg_type );

	if( action_it != action_map.end() )
	{
		return ( action_it->second )( this, data );
	}
	else
	{
		std::cout << "Unknown message type: " << msg_type << std::endl;
		return { make_ping() };
	}
}

game_logic::msg_vector game_logic::on_join( const jsoncons::json& data )
{
	std::cout << "Joined" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start( const jsoncons::json& data )
{
	std::cout << "Race started" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions( const jsoncons::json& data )
{
	return { make_throttle( 0.6 ) };
}

game_logic::msg_vector game_logic::on_crash( const jsoncons::json& data )
{
	std::cout << "Someone crashed" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end( const jsoncons::json& data )
{
	std::cout << "Race ended" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_error( const jsoncons::json& data )
{
	std::cout << "Error: " << data.to_string() << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_your_car( const jsoncons::json& data )
{
	std::cout << "Your Car: " << data.to_string() << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init( const jsoncons::json& data )
{
	std::cout << "Game init" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_spawn( const jsoncons::json& data )
{
	std::cout << "Spawn" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_lap_finished( const jsoncons::json& data )
{
	std::cout << "Lap finished" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_finish( const jsoncons::json& data )
{
	std::cout << "Finished" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_tournament_end( const jsoncons::json& data )
{
	std::cout << "Tournament end" << std::endl;
	return { make_ping() };
}
